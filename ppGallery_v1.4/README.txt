

 ____  ____     ____  __    __ __   ___  __ __  __  __ 
 || \\ || \\    || \\ ||    || ||  // \\ || ||\ || (( \
 ||_// ||_//    ||_// ||    || || (( ___ || ||\\||  \\ 
 ||    ||       ||    ||__| \\_//  \\_|| || || \|| \_))
                                                       



Thanks for downloading ppGallery!

In this zip file, you'll find 3 demos:
1) index.html - default demo without any options
2) index-options.html - some different options
3) index-button.html - with no thumbnails


For more info and options and FAQ, goto http://www.ppplugins.com/downloads/ppgallery.php