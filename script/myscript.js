/*
 * JQuery script for HCI Project 2012 - by Nathan Power - 2nd Yr B.Sc SSD
 */
$("document").ready(function()  {
	
	
	var countCorrect = 0;
	var questionCount = 0;
	
	$("#bl1").mouseenter(function() {
				$("#bl1").css({"background-color":"#000"});	
				$("#bl1 span").css({"color":"#fff"});	
		});
		
	$("#bl1").mouseleave(function() {
				$("#bl1").css({"background-color":"#fff"});	
				$("#bl1 span").css({"color":"#000"});
		});
		
	$("#bl2").mouseenter(function() {
				$("#bl2").css({"background-color":"#000"});	
				$("#bl2 span").css({"color":"#fff"});	
		});
		
	$("#bl2").mouseleave(function() {
				$("#bl2").css({"background-color":"#fff"});	
				$("#bl2 span").css({"color":"#000"});
		});

	
	$('input:radio').screwDefaultButtons({
        image: 'url("images/checkbox30.jpg")',
        width: 30,
        height: 30 
    });
    

	$('.infoArea p').hide();
	$('.infoArea p').fadeIn(2000, function() {		
	});
	

	var originalContent = $(".infoArea p").text();
	//var originalContent = "";
	
/////////////////////////////////////////////////////////////////////////////////////////////////	
	$("#button1").mouseenter(function() {
			$(".infoArea p").replaceWith("<p>Learn about the history of the Olympic Games!</p>");
			$(".infoArea p").css({"color":"#0084c5"});		
	});
		 
	$("#button1").mouseleave (function(){
		$(".infoArea p").replaceWith("<p>"+originalContent+"</p>");
		$(".infoArea p").css({"color":"#0084c5"});
		$('.infoArea p').hide();
		$('.infoArea p').fadeIn(2000, function() {		
		});
	});
/////////////////////////////////////////////////////////////////////////////////////////////////		
	$("#button2").mouseenter(function() {
			$(".infoArea p").replaceWith("<p>Learn about the history of the Paralympic Games!</p>");
			$(".infoArea p").css({"color":"#000000"});		
	});
		 
	$("#button2").mouseleave (function(){
		$(".infoArea p").replaceWith("<p>"+originalContent+"</p>");
		$(".infoArea p").css({"color":"#000000"});
		$('.infoArea p').hide();
		$('.infoArea p').fadeIn(2000, function() {		
		});
	});
/////////////////////////////////////////////////////////////////////////////////////////////////
		$("#button3").mouseenter(function() {
			$(".infoArea p").replaceWith("<p>Learn about the some events in the Paralympics!</p>");
			$(".infoArea p").css({"color":"#e10024"});		
	});
		 
	$("#button3").mouseleave (function(){
		$(".infoArea p").replaceWith("<p>"+originalContent+"</p>");
		$(".infoArea p").css({"color":"#e10024"});
		$('.infoArea p').hide();
		$('.infoArea p').fadeIn(2000, function() {		
		});
	});
/////////////////////////////////////////////////////////////////////////////////////////////////
		$("#button4").mouseenter(function() {
			$(".infoArea p").replaceWith("<p>Facts and figures about the 2012 Paralympics!</p>");
			$(".infoArea p").css({"color":"#f2c500"});		
	});
		 
	$("#button4").mouseleave (function(){
		$(".infoArea p").replaceWith("<p>"+originalContent+"</p>");
		$(".infoArea p").css({"color":"#f2c500"});
		$('.infoArea p').hide();
		$('.infoArea p').fadeIn(2000, function() {		
		});
	});
/////////////////////////////////////////////////////////////////////////////////////////////////
		$("#button5").mouseenter(function() {
			$(".infoArea p").replaceWith("<p>Play the Paralympic quiz game!</p>");
			$(".infoArea p").css({"color":"#009f3f"});		
	});
		 
	$("#button5").mouseleave (function(){
		$(".infoArea p").replaceWith("<p>"+originalContent+"</p>");
		$(".infoArea p").css({"color":"#009f3f"});
		$('.infoArea p').hide();
		$('.infoArea p').fadeIn(2000, function() {		
		});
	});
/////////////////////////////////////////////////////////////////////////////////////////////////

/*quiz button1
 * 
 *  
 */

	for (var i=1; i<7; i++) {

		setupQuizButtons(i);
	}
	
	
	var q1Click = function() { quizButtonClick (1,2,3,4,5,6,0,0);};
	var q2Click = function() { quizButtonClick (2,1,3,4,5,6,0,0);};
	var q3Click = function() { quizButtonClick (3,1,2,4,5,6,0,0);};
	var q4Click = function() { quizButtonClick (4,1,2,3,5,6,-200,0);};
	var q5Click = function() { quizButtonClick (5,1,2,3,4,6,-200,0);};
	var q6Click = function() { quizButtonClick (6,1,2,3,4,5,-200,0);};
	
	$("#q1").click(q1Click);
	$("#q2").click(q2Click);
	$("#q3").click(q3Click);
	$("#q4").click(q4Click);
	$("#q5").click(q5Click);
	$("#q6").click(q6Click);
	
	
	

//////////////////////////////////////////////////////////////////////////////

/* Quiz validation
 * 
 */	
//Button 1 validation	
var q1Val = function () { quizValidation(1,2,3,4,5,6,0,0,"Olympia");};
var q2Val = function () { quizValidation(2,1,3,4,5,6,0,305,"Rome");};
var q3Val = function () { quizValidation(3,1,2,4,5,6,0,610,"WiSu");};
var q4Val = function () { quizValidation(4,1,2,3,5,6,0,0,"164");};
var q5Val = function () { quizValidation(5,1,2,3,4,6,0,305,"16");};
var q6Val = function () { quizValidation(6,1,2,3,4,5,0,610,"Rio");};

$(".question1").click(q1Val);
$(".question2").click(q2Val);
$(".question3").click(q3Val);
$(".question4").click(q4Val);
$(".question5").click(q5Val);
$(".question6").click(q6Val);
/*
Gallery
*/

var galleryArray = new Array(16);
var caption = new Array(16);
var galleryCount = 0;

galleryArray[0] = "jump_2328341k.jpg";
galleryArray[1] = "krid_2332445k.jpg";
galleryArray[2] = "long_2328633k.jpg";
galleryArray[3] = "katyshev_2332442k.jpg";
galleryArray[4] = "weir_2332458k.jpg";
galleryArray[5] = "shuker_2332456k.jpg";
galleryArray[6] = "straight_epa_2327099k.jpg";
galleryArray[7] = "pisto_2333005k.jpg";
galleryArray[8] = "whitehead2_2327455k.jpg";
galleryArray[9] = "cockroft_2331898k.jpg";
galleryArray[10] = "grebe_2332956k.jpg";
galleryArray[11] = "lu_dong_2328466k.jpg";
galleryArray[12] = "mamolos_2333009k.jpg";
galleryArray[13] = "murderball_2332448k.jpg";
galleryArray[14] = "fence2_2333008k.jpg";
galleryArray[15] = "opening_2332451k.jpg";

caption[0] = "Paralympic high-jumpers develop explosive power with a single leg";
caption[1] = "Setting sun: Tunisia's Mohamed Ali Krid makes a throw in the men's javelin";
caption[2] = "Visually impaired long jumpers are guided on their run-up by a guide clapping  - who then steps aside and shouts when it is time to jump";
caption[3] = "Ruslan Katyshev of Ukraine in the Men's Long Jump - F11 final";
caption[4] = "Britain's David Weir races en route to winning the men's 1500m T54";
caption[5] = "Lucy Shuker and Jordanne Whiley of Great Britain compete in the Women's Doubles Wheelchair Tennis Quarterfinal ";
caption[6] = "South Africa's Oscar Pistorius was favourite to win the 200m, but was beaten in a shock finish by Brazil's Alan Oliveira";
caption[7] = "Oscar Pistorius congratulating Alan Oliveira after his shock win";
caption[8] = "Richard Whitehead of Great Britain celebrates winning gold in the Men's 200m";
caption[9] = "Britain's Hannah Cockroft winning the T34 2oom wheelchair race, setting a paralympic record of 31.90sec";
caption[10] = "Germany's Stephaine Grebe competing in the bronze medal match of the women's singles";
caption[11] = "China's Lu Dong grips a towel with her teeth as she prepares to start the women's S6 100m backstroke";
caption[12] = "Pavlos Mamalos of Greece competes in the Men's -90 kg Powerlifting";
caption[13] = "Australia's Ryley Batt is tackled by Sweden's Per Johan Uhlmann in a pool B wheelchair rugby match";
caption[14] = "Dagmara Witos-Eze of Poland (L) in action with Pui Shan Fan of Hong Kong, China during the Women's Team Wheelchair Fencing";
caption[15] = "Sound and fury: fireworks during the opening ceremony";


$(".galleryNext").click(function() {
				galleryCount++;
				$("#galleryPhoto").attr("src","images/gallery/"+galleryArray[galleryCount]);
					
					$(".infoArea p").replaceWith("<p>"+caption[galleryCount]+"</p>"); 
					$(".infoArea p").hide(); 
					$('.infoArea p').fadeIn(2000, function() {		
						});
				if (galleryCount==15) {
					galleryCount = -1;
				}
		});

function setupQuizButtons (i){
	
	$("#q"+i).mouseenter(function() {
				$("#q"+i).css({"background-color":"#fef200"});	
				$("#q"+i).css({"cursor":"pointer"});	
		});
		
		$("#q"+i).mouseleave(function() {
				$("#q"+i).css({"background-color":"#fff"});	
		});
	
}

function checkAnswers() {
	
	if (questionCount == 6){
		if(countCorrect>2){
		$(".infoArea p").replaceWith("<p>You have answered "+countCorrect+
		" out of 6 correctly! Well done!<p>");
		}
		else {
		$(".infoArea p").replaceWith("<p>You have only answered "+countCorrect+
		" out of 6 correctly!<p>");	
		}
	}
}

function quizButtonClick(qNo,a,b,c,d,e,top,left) {
			$("#q"+qNo).unbind('click');
			$("#q"+qNo).unbind('mouseenter').unbind('mouseleave');
			$("#q"+qNo).css({"background-color":"#fff"});	
			$("#q"+qNo).css({"cursor":"auto"});
			$("#q"+qNo+" .smallCorrect").css({"visibility":"hidden"});
     		$("#q"+qNo+" .smallIncorrect").css({"visibility":"hidden"});
     		$(".smallIncorrect").css({"z-index":-5});
     		$(".smallCorrect").css({"z-index":-5});

						$("#q"+a).css({"display":"none"});
						$("#q"+b).css({"display":"none"});
						$("#q"+c).css({"display":"none"});
						$("#q"+d).css({"display":"none"});
						$("#q"+e).css({"display":"none"});
 
			$('.infoArea p').hide();
			$("#q"+qNo).stop().animate({
			"top": top,
			"left": left,
            "width": 910,
            "height": 380
     	}, 750 , function() {	
     		$("#question"+qNo+"Text").css({"visibility":"visible"});	
     		$("#answers"+qNo).css({"visibility":"visible"});
			$("input:radio").screwDefaultButtons('uncheck');
			$("input:radio").screwDefaultButtons('enable');
			$(".infoArea p").replaceWith("<p>Click on the box beside the correct answer!</p>");
			/*
			$('.infoArea p').fadeIn(2000, function() {		
						});*/
			
		});	
}

function quizValidation (qNo, a, b, c, d, e, top, left,answer){  
	$(".question"+qNo).unbind('click');
	questionCount++;
    if ($(".question"+qNo+":checked").val() == answer) {
    	$(".result").css({"background":"url(images/tick_correct.png) no-repeat center center"});
        $(".result").css({"visibility":"visible"});	
		countCorrect++;
       	$(".infoArea p").replaceWith("<p>Correct ! Well done.</p>");
       	$(".infoArea p").css({"color":"#009f3f"}); 	
   		$('.infoArea p').hide();
		$('.infoArea p').fadeIn(750, function() { 
			$(".infoArea p").replaceWith("<p>Click on a question !</p>");
			$(".infoArea p").css({"color":"#000"}); 
			$(".result").css({"visibility":"hidden"}); 
   			$("#question"+qNo+"Text").css({"visibility":"hidden"});	
    		$("#answers"+qNo).css({"visibility":"hidden"});
    		$("#q"+qNo).stop().animate({
    			 "top": top,
    			 "left": left,
           		 "width": "33%",
           		 "height": "100%"
     			}, 750 , function() {
     		$("#q"+qNo+ " .smallCorrect").css({"visibility":"visible"});
     		$("#q"+qNo+ " .smallIncorrect").css({"visibility":"hidden"});
     		$(".smallIncorrect").css({"z-index":5});
	     	$(".smallCorrect").css({"z-index":5});
						
						$("#q"+a).css({"display":"inline"});
						$("#q"+b).css({"display":"inline"});
						$("#q"+c).css({"display":"inline"});
						$("#q"+d).css({"display":"inline"});
						$("#q"+e).css({"display":"inline"});
						checkAnswers();
			}); 
			
			
    			
     		});	    	
			
     }
    else {
        $(".result").css({"background":"url(images/incorrect.png) no-repeat center center"});
        $(".result").css({"visibility":"visible"});	
		$(".infoArea p").replaceWith("<p>Incorrect ! Hard Luck !</p>");	
		$(".infoArea p").css({"color":"#e10024"});
       	$('.infoArea p').hide();
		$('.infoArea p').fadeIn(750, function() { 
			$(".infoArea p").replaceWith("<p>Click on a question !</p>");
			$(".infoArea p").css({"color":"#000"}); 
			
		$(".result").css({"visibility":"hidden"}); 
   		$("#question"+qNo+"Text").css({"visibility":"hidden"});	
    	$("#answers"+qNo).css({"visibility":"hidden"});
    	$("#q"+qNo).stop().animate({
    		"top": top,
    		"left": left,
            "width": "33%",
            "height": "100%"
     	}, 750 , function() {
     		$("#q"+qNo+" .smallIncorrect").css({"visibility":"visible"});
     		$("#q"+qNo+" .smallCorrect").css({"visibility":"hidden"});
     		$(".smallIncorrect").css({"z-index":5});
	     	$(".smallCorrect").css({"z-index":5});
			
						$("#q"+a).css({"display":"inline"});
						$("#q"+b).css({"display":"inline"});
						$("#q"+c).css({"display":"inline"});
						$("#q"+d).css({"display":"inline"});
						$("#q"+e).css({"display":"inline"});	
						checkAnswers();	
     		  }); 
       	});	 	
	
}
       
       
};


});



